package pt.isel.forkit.client;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Request.Method;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import android.content.Context;
import android.util.Log;

import pt.isel.forkit.R;
import pt.isel.forkit.models.Food;
import pt.isel.forkit.models.Offer;
import pt.isel.forkit.models.Restaurant;

//async function class
public class ForkitServiceClient {
	//TODO
	
	protected static JSONArray getArray(String urlString){
		final List<Food> fs = new ArrayList<Food>();

		HttpURLConnection con = null;
		String response = null;
		try {
			URL url = new URL(urlString);
			con = (HttpURLConnection) url.openConnection();

			InputStream in = new BufferedInputStream(con.getInputStream());

			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			StringBuilder sb = new StringBuilder();

			String line = null;
			try {
				while ((line = reader.readLine()) != null) {
					sb.append((line + "\n"));
				}
			} catch (IOException e) {
				Log.e("Forkit","error building response string from InputStream");
				return null;
			} finally {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			response = sb.toString();
		} catch (IOException e) {
			Log.e("Forkit","could not get http response");
			e.printStackTrace();
		}finally {
			con.disconnect();
		}

		JSONArray jos = null;
		
		try {
			Log.i("Forkit","Obtained JSON Object");
			jos = new JSONArray(response);
		} catch (JSONException e) {
			Log.w("Forkit", "An exception occured while reading from JSON Object");
			e.printStackTrace();
		}

		return jos;
	}
	
	public static String[] GetFood(String region){
		return new String[]{"Pastel de Nata",
							"Bola de Berlim",
							"Mil Folhas",
							"Palmier",
							"Jesu�ta",
							"Queque",
							"Bolo de Arroz",
							"Pastel de Nata",
							"Bola de Berlim",
							"Mil Folhas",
							"Palmier",
							"Jesu�ta",
							"Queque",
							"Bolo de Arroz"};
	}
	
	//TODO
	public static Food[] GetFoodFromRegion(String region){
		return new Food[]{new Food("Pastel de Nata", "O pastel de nata , is a Portuguese egg tart pastry","lisbon", R.drawable.pastel_de_nata),
				new Food("Bola de Berlim", "O pastel de nata , is a Portuguese egg tart pastry","lisbon",R.drawable.bola_de_berlim),
				new Food("Mil Folhas", "O pastel de nata , is a Portuguese egg tart pastry","lisbon",R.drawable.mil_folhas),
				new Food("Palmier", "O pastel de nata , is a Portuguese egg tart pastry","lisbon",R.drawable.palmier),
				new Food("Guardanapo","O guardanapo � um bolo que � deveras saboroso","lisbon",R.drawable.guardanapo),
				new Food("Jesu�ta", "O pastel de nata , is a Portuguese egg tart pastry","lisbon",R.drawable.jesuita),
				new Food("Queque", "O pastel de nata , is a Portuguese egg tart pastry","lisbon",R.drawable.queque),
				new Food("Bolo de Arroz", "O pastel de nata , is a Portuguese egg tart pastry","lisbon",R.drawable.bolo_de_arroz),
				new Food("Pastel de Nata", "O pastel de nata , is a Portuguese egg tart pastry","lisbon", R.drawable.pastel_de_nata),
				new Food("Bola de Berlim", "O pastel de nata , is a Portuguese egg tart pastry","lisbon",R.drawable.bola_de_berlim),
				new Food("Mil Folhas", "O pastel de nata , is a Portuguese egg tart pastry","lisbon",R.drawable.mil_folhas),
				new Food("Palmier", "O pastel de nata , is a Portuguese egg tart pastry","lisbon",R.drawable.palmier),
				new Food("Jesu�ta", "O pastel de nata , is a Portuguese egg tart pastry","lisbon",R.drawable.jesuita),
				new Food("Queque", "O pastel de nata , is a Portuguese egg tart pastry","lisbon",R.drawable.queque),
				new Food("Bolo de Arroz", "O pastel de nata , is a Portuguese egg tart pastry","lisbon",R.drawable.bolo_de_arroz)};
	}
	
	public static Offer[] GetOffersOfFood(String foodName){
		return new Offer[]{new Offer("Pastel de Nata", new Restaurant("Antiga Confeitaria de Bel�m","-9.23213","2.32131"), 4.8, "reviews"),
				new Offer("Pastel de Nata", new Restaurant("Chique de Bel�m","-9.23213","2.33131"), 4.2, "reviews"),
				new Offer("Pastel de Nata", new Restaurant("Pastelaria da Francisca","-9.23213","2.31131"), 1.9, "reviews")};
	}
	
	//TODO
	public static String GetLocation(String latitude, String longitude){
		return "Lisbon";
	}
	
	//TODO
	public static String[] GetRegionRestaurants(){
		return new String[]{"Antiga Confeitaria de Bel�m",
				"Confeitaria Nacional",
				"Periquita",
				"Chique de Bel�m"};
	}
	
	
	//TODO
	//Distance is set by the user through radio buttons
	public static String[] GetNearbyRestaurants(int distance){
		return new String[]{"Antiga Confeitaria de Bel�m",
				"Confeitaria Nacional",
				"Chique de Bel�m"};
	}

	//TODO
	public static Food[] GetFoodFromNearbyRestaurants(String lat, String lon) {
		return new Food[]{new Food("Pastel de Nata", "O pastel de nata , is a Portuguese egg tart pastry","lisbon", R.drawable.pastel_de_nata),
				new Food("Bolo de Arroz", "O pastel de nata , is a Portuguese egg tart pastry","lisbon",R.drawable.bolo_de_arroz),
				new Food("Pastel de Nata", "O pastel de nata , is a Portuguese egg tart pastry","lisbon", R.drawable.pastel_de_nata),
				new Food("Bolo de Arroz", "O pastel de nata , is a Portuguese egg tart pastry","lisbon",R.drawable.bolo_de_arroz)};
	}

	public static Food[] extractFoodArrayFromJsonRespose(JSONArray response) {
		// TODO Auto-generated method stub
		return null;
	}
}
