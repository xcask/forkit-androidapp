package pt.isel.forkit.activities;

import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import pt.isel.forkit.models.MyLocation;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;

import android.content.IntentSender;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

/**
 * Abstract class responsible to query for the device's current location. 
 * The location is obtained by using the Google Play Services.
 */
public abstract class AbstractFetchLocationActivity extends FragmentActivity implements
GooglePlayServicesClient.ConnectionCallbacks,
GooglePlayServicesClient.OnConnectionFailedListener{
	private static final String URL_REQUEST_COORDINATES = "http://forkitwebapi-1.apphb.com/api/Location/LocationByCoordinates/?lat=%.6f&lon=%.6f";
	protected static final String REQUEST_LOCATION = "Location";
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	private static final String TAG = "pt.Forkit";
	private static final CharSequence LOCATION_SETTINGS_DISABLED = "GPS/Wifi location settings are off, please turn them on and try again";
	/**
	 * Location client, used obtain device's current location.
	 */
	private LocationClient _locationClient;
	/**
	 * Location manager, used obtain device's location services status.
	 */
	private LocationManager _locationManager;
	/**
	 * Best last known device's location
	 */
	private Location _lastKnownLocation;

	protected RequestQueue _requestQueue;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		_requestQueue = Volley.newRequestQueue(this); 
		_locationClient = new LocationClient(this, this, this); 
		_locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
	}

	/**
	 * The LocationClient is queried for the last known location
	 * (current location if WiFi/GPS location retrieval settings are on)
	 */

	@Override
	protected void onStart() {
		super.onStart();
		//Checks if the GooglePlayServices are available.
		//If they are available, connects to the client,
		//else the user is shown a warning.
		if(checkPlayServices()){
			_locationClient.connect();
		}
	}

	@Override
	protected void onStop() {
		// Disconnecting the client invalidates it.
		_locationClient.disconnect();
		super.onStop();
	}

	@Override
	protected void onResume() {
		super.onResume();
		checkPlayServices();
		Toast.makeText(this, "Obtaining your current location ", Toast.LENGTH_SHORT).show();
	}

	
	//Checks if the googleplay services are available on the device
	//if not, the activity is finished.
	//finish() might not be the most correct behaviour when GPServices
	//can't get a proper answer...
	private boolean checkPlayServices() {
	    int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
	    if (resultCode != ConnectionResult.SUCCESS) {
	        if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
	            GooglePlayServicesUtil.getErrorDialog(resultCode, this,
	                    PLAY_SERVICES_RESOLUTION_REQUEST).show();
	        } else {
	            Log.i(TAG, "This device is not supported.");
	            finish();
	        }
	        return false;
	    }
	    return true;
	}


	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		/*
		 * Google Play services can resolve some errors it detects.
		 * If the error has a resolution, try sending an Intent to
		 * start a Google Play services activity that can resolve
		 * error.
		 */
		if (connectionResult.hasResolution()) {
			try {
				// Start an Activity that tries to resolve the error
				connectionResult.startResolutionForResult(
						this,
						PLAY_SERVICES_RESOLUTION_REQUEST);
				/*
				 * Thrown if Google Play services canceled the original
				 * PendingIntent
				 */
			} catch (IntentSender.SendIntentException e) {
				// Log the error
				e.printStackTrace();
			}
		}
	}
	
	@SuppressWarnings("rawtypes")
	abstract Request getRequest(MyLocation location);
	
	protected RequestQueue getRequestQueue(){
		return this._requestQueue;
	}

	
	/**
	 * This method is called when the connection with Google Play Services
	 * is established. 
	 */
	@Override
	public void onConnected(Bundle b) {
		// Display the connection status

		if(GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS ){
			_lastKnownLocation = _locationClient.getLastLocation();
		}
		if (!_locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) { 
		    Toast.makeText(this, LOCATION_SETTINGS_DISABLED, Toast.LENGTH_LONG).show();
		    return;
		}
		String url = String.format(Locale.ENGLISH, URL_REQUEST_COORDINATES, 
				_lastKnownLocation.getLatitude(),
				_lastKnownLocation.getLongitude());
		final MyLocation location = new MyLocation(_lastKnownLocation.getLatitude(),_lastKnownLocation.getLongitude());
		_requestQueue.add(new JsonObjectRequest(url,null,new Listener<JSONObject>() {	
			@Override
			public void onResponse(JSONObject response) {
				Log.i(TAG,"AbstractFetchLocation: Obtained a location name from the forkit webapi");
				String locationName = null;
				try {
					locationName = response.getString(REQUEST_LOCATION);
					location.setLocationName(locationName);
				} catch (JSONException e) {
					Log.e(TAG,"AbstractFetchLocation: Could not obtain the current location string from JSONObject");
					e.printStackTrace();
				}
				Log.i(TAG,"AbstractFetchLocation: Added a new request to obtain an item's list from the forkit webapi");
				_requestQueue.add(getRequest(location));
			}
		},new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				Log.e(TAG, "AbstractFetchLocation: Could not obtain a result\n" + error.getMessage());
			}
		}));
		
	}

	@Override
	public void onDisconnected() {
		// Display the connection status
		Toast.makeText(this, "Disconnected. Please re-connect.",
				Toast.LENGTH_SHORT).show();
	}
}
