package pt.isel.forkit.activities;

import pt.isel.forkit.R;
import pt.isel.forkit.activities.fragments.HomeFoodFragment;
import pt.isel.forkit.activities.fragments.HomeRestaurantsFragment;
import pt.isel.forkit.activities.fragments.HomeReviewsFragment;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;

//The HomeActivity's layout has a default fragment PlaceHolderFragment
//because when the method FragmentTransaction.replace() is called,
//the Fragment referenced at the attribute android:name="..." 
//for the <fragment> will still be visible, not being replaced but
//overlapped by the new Fragment
/**
 * 
 */
public class HomeActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		
		//Adapts the string array into a dropdown menu for the tab
		SpinnerAdapter sAdapter = ArrayAdapter.createFromResource(this, R.array.menu_action_list,
		          android.R.layout.simple_spinner_dropdown_item);
		
		//listener in which fragment switch occurs when the user selects an item
		ActionBar.OnNavigationListener listener = new ActionBar.OnNavigationListener(){
			String[] items = getResources().getStringArray(R.array.menu_action_list);
			@Override
			public boolean onNavigationItemSelected(int pos, long id) {
				Fragment fragment = null;
				switch (pos) {
					case 0:
						fragment = new HomeFoodFragment();
						break;
					case 1:
						fragment = new HomeRestaurantsFragment();
						break;
					case 2:
						fragment = new HomeReviewsFragment();
						break;
				}
				// Create new fragment from our own Fragment class
			    
			    FragmentTransaction ft = getFragmentManager().beginTransaction();
			    
			    //The replace() is not working as it should, so the default fragment
			    //for HomeActivity's layout is a place holder, to be "replaced" with
			    //the newly selected fragment
			    ft.replace(R.id.main_fragment_container, fragment, this.items[pos]);
			    
			    ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			    //commit's the current transaction
			    ft.commit();
			    return true;
			}
		};
		actionBar.setListNavigationCallbacks(sAdapter, listener);
		
		//After setting up the menu, user holds to get the device's current position coordinates
		
	}
}
