package pt.isel.forkit.activities;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.android.volley.toolbox.ImageLoader.ImageCache;
import com.google.android.gms.internal.c;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.MarkerOptionsCreator;

import pt.isel.forkit.R;
import pt.isel.forkit.activities.FoodGridViewActivity.ItemViewHolder;
import pt.isel.forkit.client.ForkitServiceClient;
import pt.isel.forkit.models.Food;
import pt.isel.forkit.models.Offer;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.sax.StartElementListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class FoodActivity extends Activity implements IInfoHolder<Food> {
	public static final String EXTRA_FOOD = "pt.isel.forkit.food.detaileditem";
	private static final int MAX_TITLE_LENGTH = 20;
	public static final String EXTRA_RESTAURANT = "pt.isel.forkit.food.restaurant.extra";
	private Food food;

	//TODO This EXTRA_FOOD Food should be passed down to the fragments
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent i = getIntent();
		
		this.food = (Food) i.getParcelableExtra(EXTRA_FOOD);
		ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
//		actionBar.setDisplayShowTitleEnabled(false);

		Tab tab = actionBar.newTab();
		tab.setText(R.string.overview);
		tab.setTabListener(new FoodTabListener<FoodOverviewFragment>(this,
				"overview", 
				FoodOverviewFragment.class));
		actionBar.addTab(tab);

		Tab tab2 = actionBar.newTab();
		tab2.setText(R.string.offers);
		tab2.setTabListener(new FoodTabListener<FoodOffersFragment>(this,
				"offers", 
				FoodOffersFragment.class));
		actionBar.addTab(tab2);
		String foodName = this.food.getName();
		if(foodName.length() > MAX_TITLE_LENGTH){
			foodName = foodName.substring(0, MAX_TITLE_LENGTH - 3) + "...";
		}
		actionBar.setTitle(foodName);
	}
	
	//TODO verify activity and data persistency
		@Override
		public Food getInfo() {
			return this.food;
		}
	
	//TabListener, works for both existing Fragments
	//T must extend Fragment to avoid exception when Fragment.instantiate(fragmentName) is called
	class FoodTabListener<T extends Fragment> implements ActionBar.TabListener{
		Context context;
		Class<T> fragmentClass;
		String tag;
		Fragment fragment;
		
		public FoodTabListener(Context context, String tag, Class<T> fragmentClass){
			this.context = context;
			this.tag = tag;
			this.fragmentClass = fragmentClass;
		}
		
		@Override
		public void onTabReselected(Tab tab, FragmentTransaction ft) {
			//Nothing happends when the user reselects the tab
		}

		@Override
		public void onTabSelected(Tab tab, FragmentTransaction ft) {
			// Check if the fragment is already initialized
	        if (this.fragment == null) {
	            // If not, instantiate and add it to the activity
	        	this.fragment = Fragment.instantiate(this.context, this.fragmentClass.getName());
	            ft.add(android.R.id.content, this.fragment, this.tag);
	        } else {
	            // If it exists, simply attach it in order to show it
	            ft.attach(this.fragment);
	        }
		}

		@Override
		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
			if (this.fragment != null) {
	            // Detach the fragment, because another one is being attached
	            ft.detach(this.fragment);
	        }
		}

	}

	public static class FoodOverviewFragment extends Fragment{
		private Food food;
		private ImageLoader _imageLoader;
		private RequestQueue _requestQueue;
		@Override
		public void onAttach(Activity activity) {
			super.onAttach(activity);
			IInfoHolder<Food> holder = null;
			try{
				//if(activity.getClass().isInstance(FetchableInfo))
				//TODO
				holder = (IInfoHolder<Food>) activity;
			}catch(ClassCastException e) {
		        throw new ClassCastException(activity.toString() + " must implement FetchableInfo");
		    }
			this.food = holder.getInfo();
			_requestQueue = Volley.newRequestQueue(activity);
			_imageLoader = new ImageLoader(_requestQueue, new ImageCache() {
				
				@Override
				public void putBitmap(String arg0, Bitmap arg1) { }
				
				@Override
				public Bitmap getBitmap(String arg0) { return null; }
			});
		}
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, 
				Bundle savedInstanceState) {
			// Inflate the layout for this fragment
			View view =  inflater.inflate(R.layout.food_overview, container, false);
			((TextView)view.findViewById(R.id.food_overview_name)).setText(this.food.getName());
			((TextView)view.findViewById(R.id.food_overview_description_text)).setText(this.food.getDescription());
			NetworkImageView imageView = (NetworkImageView)view.findViewById(R.id.food_overview_image);
			imageView.setLayoutParams(new RelativeLayout.LayoutParams(320,320));
			imageView.setDefaultImageResId(R.drawable.default_forkit);
			imageView.setImageUrl(this.food.getImageURL(), _imageLoader);
			return view;
		}
	}

	public static class FoodOffersFragment extends Fragment{
		private Food food;
		private GoogleMap _map;
		
		@SuppressWarnings("unchecked")
		@Override
		public void onAttach(Activity activity) {
			super.onAttach(activity);
			//The info holder reference works as an interface to allow the communication between fragments. In this
			//implementation, it's used to receive the information about a food instance (to extract its offers collection).
			IInfoHolder<Food> holder = null;
			try{
				//if(activity.getClass().isInstance(FetchableInfo))
				//TODO
				holder = (IInfoHolder<Food>) activity;
			}catch(ClassCastException e) {
		        throw new ClassCastException(activity.toString() + " must implement FetchableInfo");
		    }
			this.food = holder.getInfo();
		}
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, 
				Bundle savedInstanceState) {
			//Init fragment offers list
			View view =  inflater.inflate(R.layout.food_offers, container, false);
			TextView text = (TextView)view.findViewById(R.id.food_offers_text);
			text.setText(this.food.getName());
			
			//TODO
			//Mark the offers locations on the map
			_map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
			Offer[] offers = ForkitServiceClient.GetOffersOfFood(this.food.getName());
			ListView list = (ListView)view.findViewById(R.id.list);
			list.setAdapter(new OffersAdapter(getActivity(), R.layout.food_offers_listitem, offers));
			list.setOnItemClickListener(new OffersClickListener(offers));
			for(Offer o: offers){
				//Should be a more complex view
//				TextView t = new TextView(getActivity());
//				t.setText(o.getRestaurant().getName());
//				list.addView(t);
			}
			
			//TODO Async
			//Get offers
			//while
			//Mark all the offers occurrences on the map
			
			return view;
		}
		
		/*
		private void setUpMapIfNeeded() {
		    // Do a null check to confirm that we have not already instantiated the map.
		    if (_map == null) {
		        _map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
		                            .getMap();
		        // Check if we were successful in obtaining the map.
		        if (_map == null) {
		            Log.e("Forkit", "FoodOffers: Could not obtain the map");
		        }
		    }
		}
		*/
		
		class OffersAdapter extends ArrayAdapter<Offer>{
			Context _context;
			Offer[] _offers;
			int _resId;
			public OffersAdapter(Context context, int resource, Offer[] offers) {
				super(context, resource, offers);
				_context = context;
				_offers = offers;
				_resId = resource;
			}
			
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				ItemViewHolder refHolder = null;

				if(convertView == null){
					LayoutInflater inflater = ((Activity)_context).getLayoutInflater();
					convertView = inflater.inflate(_resId, parent, false);

					refHolder = new ItemViewHolder();
					refHolder.rating = (TextView)convertView.findViewById(R.id.restaurant);
					refHolder.restaurant = (TextView)convertView.findViewById(R.id.rating);

					//setAdjustViewBounds(boolean)
					convertView.setTag(refHolder);
				}
				else{ //time to recycle
					refHolder = (ItemViewHolder)convertView.getTag();
				}

				refHolder.restaurant.setText(_offers[position].getRestaurant().getName());
				//Fetch image from built cache?
				refHolder.rating.setText(_offers[position].getRestaurant().getName());

				return convertView;
			}
			
			
			
			
		}
	}
	
	static class OffersClickListener implements OnItemClickListener{
		Offer[] _offers;
		
		public OffersClickListener(Offer[] offers){
			_offers = offers;
		}
		@Override
		public void onItemClick(AdapterView<?> adapter, View view, int pos,
				long id) {
			Context context = adapter.getContext();
			Intent i = new Intent(context,OfferActivity.class);
			i.putExtra(EXTRA_FOOD, _offers[pos].getFoodName());
			i.putExtra(EXTRA_RESTAURANT, _offers[pos].getRestaurant().getName());
			context.startActivity(i);
		}
		
	}
	
	static class ItemViewHolder{
		TextView restaurant;
		TextView rating;
	}
}
