package pt.isel.forkit.activities;


//interface to pass data from one fragment to another
public interface IInfoHolder<T> {
	public T getInfo();
}
