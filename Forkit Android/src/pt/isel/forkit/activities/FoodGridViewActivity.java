package pt.isel.forkit.activities;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.ImageLoader.ImageCache;

import pt.isel.forkit.R;
import pt.isel.forkit.models.Food;
import pt.isel.forkit.models.MyLocation;
import android.R.bool;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

//DataPull
public class FoodGridViewActivity extends AbstractFetchLocationActivity {
	public static final String EXTRA_LIST_TYPE = "pt.isel.forkit.food.listtype";
	
	public static final int FOOD_LIST_TYPE_REGION = 0;
	public static final int FOOD_LIST_TYPE_NEARBY = 1;
	
	protected int itemDimensions = 220;
	protected int defaultRadius = 10; //in KM
	protected static final String TAG = "pt.Forkit";

	private static final String URL_REGION_REQUEST = "http://forkitwebapi-1.apphb.com/api/Food/FoodByLocation?location=%s";
	private static final String URL_NEARBY_REQUEST = "http://forkitwebapi-1.apphb.com/api/Food/FoodNearby/?lat=%.6f&lon=%.6f&radius=%d";
	
	private boolean _isRegionRequest;
	ImageLoader _imageLoader;
	GridView grid;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_food_list);
		//Set ActionBar Up button and title
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		//Depending on the food list type selected, different methods 
		//of the service client are called
		
		//getIntent()
		//getExtra()
		//ForkitServiceClient.getFood(region)
		Intent i = getIntent();
		_isRegionRequest = i.getIntExtra(EXTRA_LIST_TYPE, 0) == 0;
		
		this.grid = (GridView)findViewById(R.id.food_gridview);
		this.grid.setOnItemClickListener(new ItemClickListener());
		
		//this imagecache is to avoid image decoding of a recent request image
		//to happen again. The image is still cached though
		_imageLoader = new ImageLoader(_requestQueue, new ImageCache() {
			
			@Override
			public void putBitmap(String arg0, Bitmap arg1) { }
			
			@Override
			public Bitmap getBitmap(String arg0) { return null; }
		});
	}

	class ItemClickListener implements AdapterView.OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			Intent i = new Intent(FoodGridViewActivity.this, FoodActivity.class);
			// passing array index
			
			//TODO
			//should be a reference passed(id), rather than the value(parcelable instance) ?
			i.putExtra(FoodActivity.EXTRA_FOOD, (Food)parent.getItemAtPosition(position));
			startActivity(i);
		}
	}

	class FoodArrayAdapter extends ArrayAdapter<Food>{
		private Food[] food;
		private int resId;
		private Context context;

		public FoodArrayAdapter(Context context, int ResourceId,
				Food[] objects) {
			super(context, ResourceId, objects);
			this.context = context;
			this.food = objects;
			this.resId = ResourceId;
		}

		//ItemViewHolder holds the references to all the views of the current Item,
		//so that findViewById() doesn't have to be called more than once for each view.
		//There is one ItemViewHolder per active item.
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ItemViewHolder refHolder = null;

			if(convertView == null){
				LayoutInflater inflater = ((Activity)this.context).getLayoutInflater();
				convertView = inflater.inflate(this.resId, parent, false);

				refHolder = new ItemViewHolder();
				refHolder.subtitle = (TextView)convertView.findViewById(R.id.food_gridlist_subtitle);
				refHolder.image = (NetworkImageView)convertView.findViewById(R.id.food_gridlist_image);
				refHolder.image.setLayoutParams(new LinearLayout.LayoutParams(FoodGridViewActivity.this.itemDimensions,
						FoodGridViewActivity.this.itemDimensions));
				//setAdjustViewBounds(boolean)
				convertView.setTag(refHolder);
			}
			else{ //time to recycle !!
				refHolder = (ItemViewHolder)convertView.getTag();
			}

			refHolder.subtitle.setText(this.food[position].getName());
			//Fetch image from built cache?
			refHolder.image.setDefaultImageResId(R.drawable.default_forkit);
			refHolder.image.setImageUrl(this.food[position].getImageURL(), _imageLoader);
			return convertView;
		}
	}
	
	//references of the views that compose each item in the list, no need to use findViewById multiple times.
	//It is associated to the corresponding view's tag attribute
	static class ItemViewHolder{
		TextView subtitle;
		NetworkImageView image;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected Request getRequest(MyLocation location) {
		String url;
		if(_isRegionRequest){
			url = String.format(URL_REGION_REQUEST, 
					location.getLocationName());
		}else{
			url = String.format(Locale.ENGLISH, URL_NEARBY_REQUEST, 
					location.getLatitude(),
					location.getLongitude(), 
					defaultRadius);
		}
		
		return new JsonArrayRequest(url,new Listener<JSONArray>() {
			@Override
			public void onResponse(JSONArray response) {
				Log.i(TAG,"AbstractFetchLocation: Added a new request to obtain an item's list from the forkit webapi");
				FoodGridViewActivity.this.grid.setAdapter(new FoodArrayAdapter(FoodGridViewActivity.this,
						R.layout.food_gridlist_item,
						extractFoodArrayFromJsonRespose(response)));
			}

			private Food[] extractFoodArrayFromJsonRespose(JSONArray response) {
				List<Food> food = new ArrayList<Food>();
				for(int n=0;n < response.length(); n++){
					try {
						food.add(new Food(response.getJSONObject(n)));
					} catch (JSONException e) {
						Log.e("FORKIT", "There was an error while extracting a JSONObject from the JSONArray");
						e.printStackTrace();
					}
				}
				Food[] fs = new Food[response.length()];
				food.toArray(fs);
				return fs;
			}
		}, null);
	}
}
