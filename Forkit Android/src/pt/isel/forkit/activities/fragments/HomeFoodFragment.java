package pt.isel.forkit.activities.fragments;

import pt.isel.forkit.R;
import pt.isel.forkit.activities.FoodGridViewActivity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class HomeFoodFragment extends Fragment implements OnClickListener {
	
	//TODO
	//Should the button's listener be set here?
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, 
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
		View v = inflater.inflate(R.layout.home_food, container, false);
		((Button)v.findViewById(R.id.home_food_button_region)).setOnClickListener(this);
		((Button)v.findViewById(R.id.home_food_button_nearby)).setOnClickListener(this);
        return v;
    }

	@Override
	public void onClick(View v) {
		Intent intent = new Intent(getActivity(), FoodGridViewActivity.class);
		switch(v.getId()){
			case R.id.home_food_button_region:
				intent.putExtra(FoodGridViewActivity.EXTRA_LIST_TYPE, FoodGridViewActivity.FOOD_LIST_TYPE_REGION);
				break;
			case R.id.home_food_button_nearby:
				intent.putExtra(FoodGridViewActivity.EXTRA_LIST_TYPE, FoodGridViewActivity.FOOD_LIST_TYPE_NEARBY);
				break;
		}

		startActivity(intent);
	}
}
