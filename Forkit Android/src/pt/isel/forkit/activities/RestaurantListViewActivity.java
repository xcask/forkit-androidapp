package pt.isel.forkit.activities;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;

import pt.isel.forkit.R;
import pt.isel.forkit.models.MyLocation;
import pt.isel.forkit.models.Restaurant;

import com.android.volley.Request;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonArrayRequest;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class RestaurantListViewActivity extends AbstractFetchLocationActivity {
	private static final String URL_REGION_REQUEST = "http://forkitwebapi-1.apphb.com/api/Restaurant/FoodByLocation?location=%s";
	private static final String URL_NEARBY_REQUEST = "http://forkitwebapi-1.apphb.com/api/Restaurant/FoodNearby/?lat=%.6f&lon=%.6f&radius=%d";
	
	protected int defaultRadius = 10; //in KM
	protected static final String TAG = "pt.Forkit";
	private static final int RADIUS = 10; //in KM
	private ListView _list;
	private Boolean _isRegionRequest;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.restaurants_list);
		_list = (ListView)findViewById(R.id.list);
	}
	
	@Override
	protected Request getRequest(MyLocation location) {
		String url;
		if(_isRegionRequest){
			url = String.format(URL_REGION_REQUEST, 
					location.getLocationName());
		}else{
			url = String.format(Locale.ENGLISH, URL_NEARBY_REQUEST, 
					location.getLatitude(),
					location.getLongitude(), 
					defaultRadius);
		}

		return new JsonArrayRequest(url,new Listener<JSONArray>() {
			@Override
			public void onResponse(JSONArray response) {
				Log.i(TAG,"AbstractFetchLocation: Added a new request to obtain an item's list from the forkit webapi");
				RestaurantListViewActivity.this._list.setAdapter(new RestaurantArrayAdapter(RestaurantListViewActivity.this,
						R.layout.restaurants_list_item,
						extractRestaurantsArrayFromJsonRespose(response)));
			}

			private Restaurant[] extractRestaurantsArrayFromJsonRespose(JSONArray response) {
				List<Restaurant> restaurants = new ArrayList<Restaurant>();
				for(int n=0;n < response.length(); n++){
					try {
						//TODO Constructor for restaurants from a JSONObject
						restaurants.add(new Restaurant(response.getJSONObject(n)));
					} catch (JSONException e) {
						Log.e("FORKIT", "There was an error while extracting a JSONObject from the JSONArray");
						e.printStackTrace();
					}
				}
				Restaurant[] rs = new Restaurant[response.length()];
				restaurants.toArray(rs);
				return rs;
			}
		}, null);
	}
	
	class RestaurantArrayAdapter extends ArrayAdapter<Restaurant>{
		private Restaurant[] restaurant;
		private int resId;
		private Context context;

		public RestaurantArrayAdapter(Context context, int ResourceId,
				Restaurant[] objects) {
			super(context, ResourceId, objects);
			this.context = context;
			this.restaurant = objects;
			this.resId = ResourceId;
		}

		//ItemViewHolder holds the references to all the views of the current Item,
		//so that findViewById() doesn't have to be called more than once for each view.
		//There is one ItemViewHolder per active item.
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ItemViewHolder refHolder = null;

			if(convertView == null){
				LayoutInflater inflater = ((Activity)this.context).getLayoutInflater();
				convertView = inflater.inflate(this.resId, parent, false);

				refHolder = new ItemViewHolder();
				refHolder.name = (TextView)convertView.findViewById(R.id.restaurant_list_name);
				refHolder.address = (TextView)convertView.findViewById(R.id.restaurant_list_address);
				//setAdjustViewBounds(boolean)
				convertView.setTag(refHolder);
			}
			else{ //time to recycle
				refHolder = (ItemViewHolder)convertView.getTag();
			}

			refHolder.name.setText(this.restaurant[position].getName());
			//Fetch image from built cache?
			refHolder.address.setText("address");

			return convertView;
		}
	}
	
	//references of the views that compose each item in the list, no need to use findViewById multiple times.
	//It is associated to the corresponding view's tag attribute
	static class ItemViewHolder{
		TextView name;
		TextView address;
	}
}
