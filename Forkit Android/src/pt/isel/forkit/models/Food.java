package pt.isel.forkit.models;

import org.json.JSONException;
import org.json.JSONObject;

import pt.isel.forkit.R;

import android.os.Parcel;
import android.os.Parcelable;

public class Food implements Parcelable {
	private String name;
	private String description;
	private String location;
	private String imageURL;
	private int resImage;
	
	public Food(String name, String description, String location, int resImage){
		this.setName(name);
		this.setDescription(description);
		this.setLocation(location);
		this.setResImage(resImage);
	}
	
	public Food(Parcel parc){
		this.setName(parc.readString());
		this.setDescription(parc.readString());
		this.setImageURL(parc.readString());
	}

	public Food(JSONObject jsonObject) {
		try {
			this.name = jsonObject.getString("Name");
			this.description = jsonObject.getString("Description");
			this.imageURL = jsonObject.getString("ImageUrl");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public int getResImage() {
		return this.resImage;
	}

	public void setResImage(int resImage) {
		this.resImage = resImage;
	}
	
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getImageURL() {
		return this.imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	@Override
	public int describeContents() {	
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parc, int flags) {
		parc.writeString(this.name);
		parc.writeString(this.description);
		parc.writeString(this.imageURL);
	}
	
	public static final Parcelable.Creator<Food> CREATOR = new Parcelable.Creator<Food>() {
        public Food createFromParcel(Parcel in) {
            return new Food(in);
        }

        public Food[] newArray(int size) {
            return new Food[size];
        }
    };
}
