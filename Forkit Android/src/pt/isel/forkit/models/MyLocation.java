package pt.isel.forkit.models;

public class MyLocation {
	private double _latitude;
	private double _longitude;
	private String _locationName;
	public MyLocation(double latitude, double longitude){
		_latitude = latitude;
		_longitude = longitude;
	}
	
	public void setLocationName(String locationName){
		_locationName = locationName;
	}
	
	public double getLatitude(){
		return _latitude;
	}
	
	public double getLongitude(){
		return _longitude;
	}
	
	public String getLocationName(){
		return _locationName;
	}
}
