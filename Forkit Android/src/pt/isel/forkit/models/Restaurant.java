package pt.isel.forkit.models;

import org.json.JSONObject;

public class Restaurant {

	private String name;
	private String lat;
	private String lon;

	public Restaurant(String restaurantName, String latitude, String longitude) {
		this.setName(restaurantName);
		this.setLat(latitude);
		this.setLon(longitude);
	}

	public Restaurant(JSONObject jsonObject) {
		//Should be done with GSON
		//TODO
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLat() {
		return this.lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLon() {
		return this.lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}

}
