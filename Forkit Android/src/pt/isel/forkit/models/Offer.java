package pt.isel.forkit.models;

public class Offer {

	private String foodName;
	private Restaurant restaurant;
	private double rating;
	private String reviews;

	public Offer(String foodName, Restaurant restaurant, double d, String reviews) {
		this.setFoodName(foodName);
		this.setRestaurant(restaurant);
		this.setRating(d);
		this.setReviews(reviews);
	}

	public String getFoodName() {
		return this.foodName;
	}

	public void setFoodName(String foodName) {
		this.foodName = foodName;
	}

	public Restaurant getRestaurant() {
		return this.restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	public double getRating() {
		return this.rating;
	}

	public void setRating(double d) {
		this.rating = d;
	}

	public String getReviews() {
		return this.reviews;
	}

	public void setReviews(String reviews) {
		this.reviews = reviews;
	}
}
